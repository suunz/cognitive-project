
'use strict'
const position = (instruction, obj, map) => {
  return instruction === 'L' || instruction === 'R' ? align(instruction, obj, map) : move(obj, map)
}

const move = (obj, map) => {
  switch (obj.align) {
    case 'N':
      obj.y = map.y < obj.y + 1 ? obj.y : obj.y + 1
      break;
    case 'E':
    obj.x = map.x < obj.x + 1 ? obj.x : obj.x + 1
      break;
    case 'W':
    obj.x = obj.x - 1 < 0 ? obj.x : obj.x - 1
      break;
    case 'S':
      obj.y = obj.y - 1 < 0 ? obj.y : obj.y - 1
      break;
  }
  return obj
}

const align = (instruction, obj, map) => {
  if(instruction === 'R'){
    switch (obj.align) {
      case 'N':
        obj.align = 'E'
        break;
      case 'E':
        obj.align = 'S'
        break;
      case 'S':
        obj.align = 'W'
        break;
      case 'W':
        obj.align = 'N'
        break;
    }
  }else{
    switch (obj.align) {
      case 'N':
        obj.align = 'W'
        break;
      case 'E':
        obj.align = 'N'
        break;
      case 'S':
        obj.align = 'E'
        break;
      case 'W':
        obj.align = 'S'
        break;
    }
  }
  return obj
}

module.exports = {
  position,
  move,
  align
}
