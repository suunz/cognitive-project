const {align, move, position} = require('./movements')
const {expect} = require('chai')

let map = {}
let firstTest = {}
let secondTest = {}
let thirdTest = {}
let fourTest = {}
let test = {}

describe('Movements Functions', () => {
  describe('Position Function', () => {
    beforeEach(function () {
      test = '';
      map = { x: 5, y: 5 }
      firstTest = { align: 'N', x: 0, y: 4 }
      secondTest = { align: 'E', x: 0, y: 4 }
      thirdTest = { align: 'W', x: 0, y: 4 }
      fourTest = { align: 'S', x: 0, y: 4 }
    })
  
    it('Position First Test Left', () => {
      test = position('L', firstTest, map)
      expect(test).eql({ align: 'W', x: 0, y: 4 })
    })
    it('Position First Test Right', () => {
      test = position('R', firstTest, map)
      expect(test).eql({ align: 'E', x: 0, y: 4 })
    })
    it('Position First Test Move', () => {
      test = position('M', firstTest, map)
      expect(test).eql({ align: 'N', x: 0, y: 5 })
    })
  })
  
  describe('Move Function', () => {
    beforeEach(function () {
      test = '';
      firstTest = { align: 'N', x: 0, y: 4 }
      secondTest = { align: 'E', x: 0, y: 4 }
      thirdTest = { align: 'W', x: 0, y: 4 }
      fourTest = { align: 'S', x: 0, y: 4 }
    })
  
    it('Move First Test', () => {
      test = move(firstTest, map)
      expect(test).eql({ align: 'N', x: 0, y: 5 })
    })
    it('Not Move First Test', () => {
      firstTest = { align: 'N', x: 0, y: 5 }
      test = move(firstTest, map)
      expect(test).eql({ align: 'N', x: 0, y: 5 })
    })
    it('Move Second Test', () => {
      test = move(secondTest, map)
      expect(test).eql({ align: 'E', x: 1, y: 4 })
    })
    it('Not Move Second Test', () => {
      secondTest = { align: 'E', x: 5, y: 4 }
      test = move(secondTest, map)
      expect(test).eql({ align: 'E', x: 5, y: 4 })
    })
    it('Move Third Test', () => {
      test = move(thirdTest, map)
      expect(test).eql({ align: 'W', x: 0, y: 4 })
    })
    it('Not Move Third Test', () => {
      thirdTest = { align: 'W', x: 5, y: 4 }
      test = move(thirdTest, map)
      expect(test).eql({ align: 'W', x: 4, y: 4 })
    })
    it('Move Four Test', () => {
      test = move(fourTest, map)
      expect(test).eql({ align: 'S', x: 0, y: 3 })
    })
    it('Not Move Four Test', () => {
      fourTest = { align: 'S', x: 0, y: 0 }
      test = move(fourTest, map)
      expect(test).eql({ align: 'S', x: 0, y: 0 })
    })
  })
  
  describe('Align Function', () => {
    let test = ''
    beforeEach(function () {
      map = { x: 5, y: 5 }
      firstTest = { align: 'N', x: 0, y: 4 }
      secondTest = { align: 'E', x: 0, y: 4 }
      thirdTest = { align: 'W', x: 0, y: 4 }
      fourTest = { align: 'S', x: 0, y: 4 }
      test = null;
    })
  
    it('Align First Test Left', () => {
      test = align('L', firstTest, map)
      expect(test).eql({ align: 'W', x: 0, y: 4 })
    })
    it('Align First Test Right', () => {
      test = align('R', firstTest, map)
      expect(test).eql({ align: 'E', x: 0, y: 4 })
    })
    it('Align Second Test Left', () => {
      test = align('L', secondTest, map)
      expect(test).eql({ align: 'N', x: 0, y: 4 })
    })
    it('Align Second Test Right', () => {
      test = align('R', secondTest, map)
      expect(test).eql({ align: 'S', x: 0, y: 4 })
    })
    it('Align Third Test Left', () => {
      test = align('L', thirdTest, map)
      expect(test).eql({ align: 'S', x: 0, y: 4 })
    })
    it('Align Third Test Right', () => {
      test = align('R', thirdTest, map)
      expect(test).eql({ align: 'N', x: 0, y: 4 })
    })
    it('Align Four Test Left', () => {
      test = align('L', fourTest, map)
      expect(test).eql({ align: 'E', x: 0, y: 4 })
    })
    it('Align Four Test Right', () => {
      test = align('R', fourTest, map)
      expect(test).eql({ align: 'W', x: 0, y: 4 })
    })
  })
})