# Teste de Programação - COGTIVE

 Olá! o projeto foi feito usando NodeJS(JavaScript) ficou bem simples devido ao tempo que tive, mas funcional.

## Requisitos para rodar o projeto

```
Node - Versão recomendada 10.13.0 ou superior
NPM - Versão recomendada 6.4.1 ou superior
```

## Descrição do Projeto

O projeto foi feito como um CLI (Command-Line Interface), todos seus comandos podem ser vistos apartir da executação do projeto usando o comando **help**,
para efetuar a execução do projeto é necessario instalar as dependencias com **npm install** na pasta do projeto e executar **node main**, após isso
já será executado a função **iniciar** automáticamente onde será definido o tamanho de Marte em uma escala X Y, para definir uma nova sonda com sua posição inicial e suas instruções é necessário digitar o comando **novaSonda** é possivel adicionar quantas sondas forem necessárias, após preencher seus requisitos poderá usar o comando **movimentarSondas** o qual retornará a posição final da sonda baseado em suas instruções.
Como não foi definido como seria tratado caso alguma sonda saísse do escopo de Marte, tomei como decisão que ela detectasse que não poderia prosseguir e que só continuasse na mesma posição e a mesma direção.

## Testes

Efetuei os testes unitários sómente no arquivo movements.js escritos em movements.test.js para rodar a execução do teste é só digitar o seguinte comando no terminar **npm run test** para verificar os testes e tambem **npm run coverage** para verificar a cobertura dos testes sobre o código  (Provavelmente os comandos só funcionarão no **Windows**, não consegui testar em outro SO).
