'use strict'
const { position } = require('./movements')
const { mapQuestion, probeQuestions } = require('./questions')
const vorpal = require('vorpal')()
const Ora = require('ora')

process.stdout.write('\x1Bc')
const map = {}
const objs = [] 
vorpal
  .command('iniciar', 'Inicia o processo da descoberta do tamanho de Marte.')
    .action(function(args, callback) {
      this.prompt(mapQuestion, (answer) => {
        map.x = answer.map.split(' ')[0]
        map.y = answer.map.split(' ')[1]
        process.stdout.write('\x1Bc')
        Ora().succeed('Tamanho de Marte calculado com sucesso!')
        callback()
      })
    });

vorpal
  .command('novaSonda', 'Define uma nova Sonda e suas especificações.')
    .action(function(args, callback) {
      this.prompt(probeQuestions, (answers) => {
        process.stdout.write('\x1Bc')
        const obj = {}
        const posAndAlign = answers.posInitial.split(' ')
        obj.x = parseInt(posAndAlign[0])
        obj.y = parseInt(posAndAlign[1])
        obj.align = posAndAlign[2]
        obj.instructions = answers.instructions
        objs.push(obj)
        Ora().succeed('Sonda inviada a Marte com sucesso!')
        callback()
      })
    });

vorpal
  .command('movimentarSondas', 'Retorna a nova posição das sondas.')
    .action(function(args, callback) {
      process.stdout.write('\x1Bc')
      if(objs.length > 0){
        objs.map((obj) => {
          const instructions = obj.instructions.split('')
          instructions.forEach((instruction) => {
            return position(instruction, obj, map)  
          })
        })
        objs.forEach((obj, i) => {
          this.log(`${i + 1}ª Sonda ${obj.x} ${obj.y} ${obj.align}`)
        })
      }else{
        Ora().fail('Não possui nenhuma sonda online!')
        callback();
      }
    });

vorpal
  .command('clear')
    .description('Limpa o console.')
      .action((args, callback) => {
        process.stdout.write('\x1Bc')
        callback();
      })

vorpal.delimiter('NASA$').show();
vorpal.exec('iniciar');