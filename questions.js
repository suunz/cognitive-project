const { verifyInstruction, verifyAlign } = require('./utils')

const mapQuestion = { type: 'input', name: 'map', message: 'Insira o Tamanho de Marte X Y: ',
  validate: (arg) => {
    const error = 'Os valores devem ser X Y'
    const val = arg.split(' ')
    if (val.length > 2 || isNaN(val[0]) || isNaN(val[1])){
      return error
    }else{
      return true
    }
  }    
}

const probeQuestions = [
  {
    type: 'input',
    name: 'posInitial',
    message: 'Insira a posição inicial: ',
    validate: (arg) => {
      const error = 'Os valores devem ser X Y Alinhamento'
      const val = arg.split(' ')
      if (val.length != 3 || isNaN(val[0]) || isNaN(val[1]) || !isNaN(val[2]) || !verifyAlign(val[2])){
        return error
      }else{
        return true
      }
    }  
  },
  {
    type: 'input',
    name: 'instructions',
    message: 'Insira as instruções: ',
    validate: (arg) => {
      const error = 'Os valores só podem ser L R M'
      return verifyInstruction(arg) ? true : error
    } 
  }
]


module.exports = {
  mapQuestion,
  probeQuestions
}