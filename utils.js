
const verifyInstruction = (string) => {
  const array = string.split('')
  let res 
  array.forEach((val) => {
    if(isNaN(val)){
      res = val != 'L' && val != 'R' && val != 'M' ? false : true
    }else{
      res = false
    }
  })
  return res ? true : false
}

const verifyAlign = (string) => {
  return string != 'N' && string != 'W' && string != 'E' && string != 'S' ? false : true
}

module.exports = {
  verifyInstruction,
  verifyAlign
}